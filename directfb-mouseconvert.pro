#-------------------------------------------------
#
# Project created by QtCreator 2014-05-23T11:04:03
#
#-------------------------------------------------

QT       += core


TARGET = directfb-mouseconvert
CONFIG   += console
CONFIG   += c++11
CONFIG   += debug
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
