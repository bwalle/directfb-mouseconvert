#include <iostream>
#include <fstream>
#include <algorithm>

#include <QCoreApplication>
#include <QFile>
#include <QImage>

static void usage()
{
	std::cout
			<< "Usage: directfb-mouseconvert <mode> <input> <output>\n"
			<< "\n"
			<< "   <mode>:   either \"dfb2png\" or \"png2dfb\"\n"
			<< "   <input>:  input file (either PNG or DAT ('ARGB'))\n"
			<< "   <output>: output file (either DAT ('ARGB') or PNG)\n" << std::endl;

}

static constexpr size_t WIDTH = 40;
static constexpr size_t HEIGHT = 40;
static constexpr size_t IMGSIZE = 4;

std::ostream &operator<<(std::ostream &os, const QString &s)
{
	return os << s.toStdString();
}

static int convertPngToArgb(const QString &input, const QString &output)
{
	QImage img(input);

	if ((img.width() != WIDTH) || (img.height() != HEIGHT)) {
		std::cout << "Invalid input image size (" << img.width() << "x" << img.height() << ")" << std::endl;
		return EXIT_FAILURE;
	}

	QFile outputFile(output);
	if (!outputFile.open(QIODevice::WriteOnly)) {
		std::cout << "Unable to open output file '" << output << "': "
				  << outputFile.errorString() << std::endl;
		return EXIT_FAILURE;
	}

	for (int y = 0; y < img.height(); y++) {
		for (int x = 0; x < img.width(); x++) {
			QRgb p = img.pixel(x, y);
			char pixel[] = { char(qRed(p)),  char(qGreen(p)), char(qBlue(p)), char(qAlpha(p)) };
			outputFile.write(pixel, sizeof(p));
		}
	}


	return EXIT_SUCCESS;
}

static int convertArgbToPng(const QString &input, const QString &output)
{
	QFile inputfile(input);
	if (!inputfile.open(QIODevice::ReadOnly)) {
		std::cout << "Unable to open input file '" << input << "': "
				  << inputfile.errorString() << std::endl;
		return EXIT_FAILURE;
	}

	constexpr size_t expectedFileSize = WIDTH*HEIGHT*IMGSIZE;


	if (inputfile.size() != expectedFileSize) {
		std::cout << "File '" << input << "' has an invalid size." << std::endl;
		return EXIT_FAILURE;
	}

	QByteArray inputbytes = inputfile.readAll();
	if (inputbytes.size() != expectedFileSize) {
		std::cout << "Unable to read the whole input file. " << std::endl;
		return EXIT_FAILURE;
	}

	QImage img( reinterpret_cast<unsigned char *>(inputbytes.data()), WIDTH, HEIGHT, QImage::Format_ARGB32);
	return img.save(output) ? EXIT_SUCCESS : EXIT_FAILURE;
}

int main(int argc, char *argv[])
{
	QCoreApplication a(argc, argv);

	if (argc != 4) {
		usage();
		return EXIT_FAILURE;
	}

	QString mode = QString::fromLocal8Bit(argv[1]);
	QString input = QString::fromLocal8Bit(argv[2]);
	QString output = QString::fromLocal8Bit(argv[3]);

	if (mode == "dfb2png")
		return convertArgbToPng(input, output);
	else if (mode == "png2dfb")
		return convertPngToArgb(input, output);
	else {
		std::cout << "Invalid mode '" << mode << "'" << std::endl;
		usage();
		return EXIT_FAILURE;
	}
}
